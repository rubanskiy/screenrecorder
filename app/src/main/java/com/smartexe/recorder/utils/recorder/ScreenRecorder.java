package com.smartexe.recorder.utils.recorder;

import android.media.projection.MediaProjection;

import com.smartexe.recorder.data.model.ScreenConfig;

public interface ScreenRecorder {

    void setConfig(ScreenConfig config);

    void setProjection(MediaProjection projection);

    void setRecordSession(String session);

    void startRecord();

    void stopRecord();

    void prepareFile();

    void split();

    boolean isRecording();

}
