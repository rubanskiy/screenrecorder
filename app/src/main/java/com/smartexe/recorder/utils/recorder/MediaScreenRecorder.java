package com.smartexe.recorder.utils.recorder;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;

import com.smartexe.recorder.data.local.VideosRepository;
import com.smartexe.recorder.data.model.ScreenConfig;
import com.smartexe.recorder.data.model.Video;
import com.smartexe.recorder.utils.PathUtils;

import java.io.IOException;

public class MediaScreenRecorder implements ScreenRecorder {

    private final static String TAG = MediaScreenRecorder.class.getSimpleName();

    private MediaProjection mediaProjection;
    private MediaRecorder mediaRecorder;
    private VirtualDisplay virtualDisplay;
    private ScreenConfig screenConfig;

    private String tmpFilePath;
    private String tmpFileName;
    private String recordSession;
    private VideosRepository repository;

    private boolean isRecording;

    public MediaScreenRecorder(Context context) {
        mediaRecorder = new MediaRecorder();
        repository = new VideosRepository(context);

        tmpFileName = TAG + "_" + System.currentTimeMillis() + ".mp4";
        tmpFilePath = PathUtils.getVideosDirectory() + tmpFileName;
    }

    @Override
    public void setConfig(ScreenConfig config) {
        this.screenConfig = config;
    }

    @Override
    public void setProjection(MediaProjection projection) {
        this.mediaProjection = projection;
    }

    @Override
    public void setRecordSession(String session) {
        this.recordSession = session;
    }

    @Override
    public void startRecord() {
        if (mediaRecorder == null) {
            throw new IllegalStateException("MediaRecorder should be provided");
        }

        prepareRecord();

        isRecording = true;
        mediaRecorder.start();

        repository.add(new Video(tmpFileName, tmpFilePath, recordSession));
    }

    @Override
    public void stopRecord() {
        if (mediaRecorder == null) {
            throw new IllegalStateException("MediaRecorder should be provided");
        }

        if (!isRecording) {
            return;
        }
        isRecording = false;
        mediaRecorder.stop();
        mediaRecorder.reset();
        virtualDisplay.release();
    }

    @Override
    public void prepareFile() {
        tmpFileName = TAG + "_" + System.currentTimeMillis() + ".mp4";
        tmpFilePath = PathUtils.getVideosDirectory() + tmpFileName;
    }

    @Override
    public void split() {
        if (mediaRecorder == null) {
            throw new IllegalStateException("MediaRecorder should be provided");
        }

        stopRecord();
        startRecord();
    }

    @Override
    public boolean isRecording() {
        return isRecording;
    }

    private void prepareRecord() {
        if (isRecording) {
            return;
        }

        initRecorder();
        createVirtualDisplay();
    }

    private void createVirtualDisplay() {
        virtualDisplay = mediaProjection.createVirtualDisplay(
                TAG, screenConfig.getWidth(), screenConfig.getHeight(), screenConfig.getDensity(),
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                mediaRecorder.getSurface(), null, null);
    }

    private void initRecorder() {
//        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(android.media.MediaRecorder.VideoSource.SURFACE);
        mediaRecorder.setOutputFormat(android.media.MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setOutputFile(tmpFilePath);
        mediaRecorder.setVideoSize(screenConfig.getWidth(), screenConfig.getHeight());
        mediaRecorder.setVideoEncoder(android.media.MediaRecorder.VideoEncoder.H264);
//        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mediaRecorder.setVideoEncodingBitRate(5 * 1024 * 1024);
        mediaRecorder.setVideoFrameRate(30);
        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
