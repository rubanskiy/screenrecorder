package com.smartexe.recorder.utils;

import android.app.ActivityManager;
import android.content.Context;

import com.smartexe.recorder.service.RecordService;

public class ServiceUtils {

    public static boolean isRecorderRunning(Context context) {
        return isServiceRunning(context, RecordService.class);
    }

    private static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

}
