package com.smartexe.recorder.utils.recorder;

import android.content.Context;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.media.projection.MediaProjection;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;

import com.smartexe.recorder.data.local.VideosRepository;
import com.smartexe.recorder.data.model.ScreenConfig;
import com.smartexe.recorder.data.model.Video;
import com.smartexe.recorder.utils.PathUtils;

import java.io.IOException;
import java.nio.ByteBuffer;

public class CodecScreenRecorder implements ScreenRecorder {

    private final static String TAG = CodecScreenRecorder.class.getSimpleName();
    private final static long KEY_FRAME_DELAY = 500;

    private MediaProjection mediaProjection;
    private ScreenConfig screenConfig;

    private VideosRepository repository;

    private boolean isRecording;
    private boolean shouldProcessData;
    private Surface surface;
    private MediaMuxer muxer;
    private MediaCodec mediaCodec;
    private MediaCodec.BufferInfo bufferInfo;
    private int trackIndex = -1;
    private String tmpFilePath;
    private String tmpFileName;
    private String recordSession;

    private ByteBuffer duplicateBytes;
    private int duplicateIndex;
    private MediaCodec.BufferInfo duplicateInfo;

    private long writeTimeStamp;

    private Handler writeDataHandler;

    public CodecScreenRecorder(Context context) {
        writeDataHandler = new Handler(Looper.getMainLooper());
        repository = new VideosRepository(context);
    }

    @Override
    public void setConfig(ScreenConfig config) {
        screenConfig = config;
    }

    @Override
    public void setProjection(MediaProjection projection) {
        this.mediaProjection = projection;
    }

    @Override
    public void setRecordSession(String session) {
        this.recordSession = session;
    }

    @Override
    public void startRecord() {
        bufferInfo = new MediaCodec.BufferInfo();
        MediaFormat format = MediaFormat.createVideoFormat("video/avc",
                screenConfig.getWidth(), screenConfig.getHeight());
        int frameRate = 30;
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
        format.setInteger(MediaFormat.KEY_BIT_RATE, 5 * 1024 * 1024);
        format.setInteger(MediaFormat.KEY_FRAME_RATE, frameRate);
        format.setInteger(MediaFormat.KEY_CAPTURE_RATE, frameRate);
        format.setInteger(MediaFormat.KEY_REPEAT_PREVIOUS_FRAME_AFTER, 1000000 / frameRate);
        format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 1);
        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1);

        try {
            mediaCodec = MediaCodec.createEncoderByType("video/avc");
            mediaCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            surface = mediaCodec.createInputSurface();
            mediaCodec.start();

            String fileName = TAG + "_" + System.currentTimeMillis() + ".mp4";
            String path = PathUtils.getVideosDirectory() + fileName;
            muxer = new MediaMuxer(path, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            Log.d(TAG, "Video file is created: " + path);

            repository.add(new Video(fileName, path, recordSession));
        } catch (IOException e) {
            stopRecord();
        }
        mediaProjection.createVirtualDisplay(TAG, screenConfig.getWidth(),
                screenConfig.getHeight(), screenConfig.getDensity(), 0 ,
                surface, null , null );

        shouldProcessData = true;

        writeData();
    }

    @Override
    public void stopRecord() {
        writeDataHandler.removeCallbacks(writeDataRunnable);
        try {
            if (muxer != null) {
                if (isRecording) {
                    muxer.stop();
                }
                muxer.release();
                muxer = null;
                isRecording = false;
            }
            if (mediaCodec != null) {
                mediaCodec.stop();
                mediaCodec.release();
                mediaCodec = null;
            }
            if (surface != null) {
                surface.release();
                surface = null;
            }
            if (mediaProjection != null) {
                mediaProjection.stop();
                mediaProjection = null;
            }

            bufferInfo = null;
            writeDataRunnable = null;
            trackIndex = -1;
            shouldProcessData = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void prepareFile() {
        tmpFileName = TAG + "_" + System.currentTimeMillis() + ".mp4";
        tmpFilePath = PathUtils.getVideosDirectory() + tmpFileName;
        Log.d(TAG, "Video file is prepared: " + tmpFilePath);
    }

    @Override
    public void split() {
        if (muxer != null) {
            if (isRecording) {
                try {
                    muxer.writeSampleData(duplicateIndex, duplicateBytes, duplicateInfo);

                    muxer.stop();
                    muxer.release();
                    muxer = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            isRecording = false;
        }

        try {
            muxer = new MediaMuxer(tmpFilePath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            trackIndex = muxer.addTrack(mediaCodec.getOutputFormat());
            muxer.start();
            isRecording = true;

            writeTimeStamp = System.currentTimeMillis();
            muxer.writeSampleData(duplicateIndex, duplicateBytes, duplicateInfo);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        repository.add(new Video(tmpFileName, tmpFilePath, recordSession));
    }

    @Override
    public boolean isRecording() {
        return isRecording;
    }

    private void writeData() {
        writeDataHandler.removeCallbacks(writeDataRunnable);
        while (shouldProcessData) {
            int bufferIndex = mediaCodec.dequeueOutputBuffer(bufferInfo, 0);

            if (bufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                if (System.currentTimeMillis() - writeTimeStamp > KEY_FRAME_DELAY && duplicateBytes != null) {
                    writeTimeStamp = System.currentTimeMillis();
                    muxer.writeSampleData(duplicateIndex, duplicateBytes, duplicateInfo);
                }
                break;
            } else if (bufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                if (trackIndex >= 0) {
                    throw new RuntimeException("format changed twice");
                }
                trackIndex = muxer.addTrack(mediaCodec.getOutputFormat());

                if (!isRecording && trackIndex >= 0) {
                    muxer.start();
                    isRecording = true;
                }
            } else if (bufferIndex >= 0) {
                ByteBuffer bytes = mediaCodec.getOutputBuffer(bufferIndex);
                if (bytes == null) {
                    throw new RuntimeException("couldn't fetch buffer at index " + bufferIndex);
                }

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    bufferInfo.size = 0;
                }

                if (bufferInfo.size != 0) {
                    if (isRecording) {
                        bytes.position(bufferInfo.offset);
                        bytes.limit(bufferInfo.offset + bufferInfo.size);

                        duplicateBytes = bytes.duplicate();
                        duplicateInfo = bufferInfo;
                        duplicateIndex = trackIndex;

                        writeTimeStamp = System.currentTimeMillis();

                        muxer.writeSampleData(trackIndex, bytes, bufferInfo);
                    }
                }

                mediaCodec.releaseOutputBuffer(bufferIndex, false);
                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    break;
                }
            }
        }
        writeDataHandler.post(writeDataRunnable);
    }

    private Runnable writeDataRunnable = new Runnable() {
        @Override
        public void run() {
            writeData();
        }
    };

}
