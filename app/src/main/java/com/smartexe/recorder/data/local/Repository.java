package com.smartexe.recorder.data.local;

import java.util.List;

public interface Repository<T> {

    void add(T item);

    List<T> getAll();

    List<T> query(String query);

    void update(T item);

    void clearAll();

}
