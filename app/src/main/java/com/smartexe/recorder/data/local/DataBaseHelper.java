package com.smartexe.recorder.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.smartexe.recorder.data.model.Video;

import java.util.ArrayList;
import java.util.List;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String VIDEO_DATABASE = "VIDEO_DATABASE";
    private static final String VIDEO_TABLE = "VIDEO_TABLE";
    private static final String VIDEO_ID = "ID";
    private static final String VIDEO_NAME = "NAME";
    private static final String VIDEO_PATH = "PATH";
    private static final String VIDEO_SESSION = "SESSION";
    private static final String VIDEO_UPLOADED = "UPLOADED";

    // package private access
    DataBaseHelper(Context context) {
        super(context, VIDEO_DATABASE, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + VIDEO_TABLE + " ("
                + VIDEO_ID + " integer primary key autoincrement,"
                + VIDEO_NAME + " text,"
                + VIDEO_PATH + " text,"
                + VIDEO_SESSION + " text,"
                + VIDEO_UPLOADED + " text"
                + "); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Not relevant for now
    }

    void add(Video video) {
        ContentValues cv = new ContentValues();
        cv.put(VIDEO_NAME, video.getName());
        cv.put(VIDEO_PATH, video.getPath());
        cv.put(VIDEO_SESSION, video.getSession());
        cv.put(VIDEO_UPLOADED, 0);
        getWritableDatabase().insert(VIDEO_TABLE, null, cv);
        close();
    }

    List<Video> getAll() {
        Cursor c = getWritableDatabase().query(
                VIDEO_TABLE, null, null, null,
                null, null, null);
        return collectVideos(c);
    }

    List<Video> getBySession(String session) {
        Cursor c = getWritableDatabase().query(
                VIDEO_TABLE, null, VIDEO_SESSION + " = ? ", new String[]{session},
                null, null, null);
        return collectVideos(c);
    }

    void update(Video video) {
        ContentValues cv = new ContentValues();
        cv.put(VIDEO_NAME, video.getName());
        cv.put(VIDEO_PATH, video.getPath());
        cv.put(VIDEO_SESSION, video.getSession());
        cv.put(VIDEO_UPLOADED, video.getUploaded() ? "1" : "0");

        getWritableDatabase().update(VIDEO_TABLE, cv,
                VIDEO_ID + " = ? ", new String[] {video.getId()});
        close();
    }

    private List<Video> collectVideos(Cursor c) {
        List<Video> videos = new ArrayList<>();
        if (c.moveToFirst()) {
            int idIndex = c.getColumnIndex(VIDEO_ID);
            int nameIndex = c.getColumnIndex(VIDEO_NAME);
            int pathIndex = c.getColumnIndex(VIDEO_PATH);
            int sessionIndex = c.getColumnIndex(VIDEO_SESSION);
            int uploadedIndex = c.getColumnIndex(VIDEO_UPLOADED);
            do {
                videos.add(new Video(c.getString(idIndex), c.getString(nameIndex),
                        c.getString(pathIndex), c.getString(sessionIndex),
                        c.getString(uploadedIndex).equals("1")));
            } while (c.moveToNext());
        }
        c.close();
        close();
        return videos;
    }

    void clear() {
        getWritableDatabase().delete(VIDEO_TABLE, null, null);
        close();
    }

}
