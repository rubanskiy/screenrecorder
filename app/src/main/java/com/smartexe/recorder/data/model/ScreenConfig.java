package com.smartexe.recorder.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ScreenConfig implements Parcelable {

    private int width;
    private int height;
    private int density;

    public ScreenConfig() {
    }

    public ScreenConfig(int width, int height, int density) {
        this.width = width;
        this.height = height;
        this.density = density;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getDensity() {
        return density;
    }

    public void setDensity(int density) {
        this.density = density;
    }

    protected ScreenConfig(Parcel in) {
        width = in.readInt();
        height = in.readInt();
        density = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeInt(density);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ScreenConfig> CREATOR = new Parcelable.Creator<ScreenConfig>() {
        @Override
        public ScreenConfig createFromParcel(Parcel in) {
            return new ScreenConfig(in);
        }

        @Override
        public ScreenConfig[] newArray(int size) {
            return new ScreenConfig[size];
        }
    };

}
