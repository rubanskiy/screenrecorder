package com.smartexe.recorder.data.model;

public class Video {

    private String id;
    private String name;
    private String path;
    private String session;
    private boolean isUploaded;

    public Video() {
    }

    public Video(String name, String path, String session) {
        this.name = name;
        this.path = path;
        this.session = session;
    }

    public Video(String id, String name, String path, String session, boolean isUploaded) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.session = session;
        this.isUploaded = isUploaded;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public boolean getUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean isUploaded) {
        this.isUploaded = isUploaded;
    }

}
