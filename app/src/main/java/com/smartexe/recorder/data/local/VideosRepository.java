package com.smartexe.recorder.data.local;

import android.content.Context;

import com.smartexe.recorder.data.model.Video;

import java.util.List;

public class VideosRepository implements Repository<Video> {

    private DataBaseHelper dataBaseHelper;

    public VideosRepository(Context context) {
        dataBaseHelper = new DataBaseHelper(context);
    }

    @Override
    public void add(Video video) {
        dataBaseHelper.add(video);
    }

    @Override
    public List<Video> getAll() {
        return dataBaseHelper.getAll();
    }

    @Override
    public List<Video> query(String query) {
        return dataBaseHelper.getBySession(query);
    }

    @Override
    public void update(Video video) {
        dataBaseHelper.update(video);
    }

    @Override
    public void clearAll() {
        dataBaseHelper.clear();
    }

}
