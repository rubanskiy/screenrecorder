package com.smartexe.recorder.service;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.smartexe.recorder.service.S3Action.ACTION_NOT_DEFINED;
import static com.smartexe.recorder.service.S3Action.ACTION_SMT_ELSE;
import static com.smartexe.recorder.service.S3Action.ACTION_UPLOAD_VIDEO;

@Retention(RetentionPolicy.SOURCE)
@IntDef({ACTION_NOT_DEFINED, ACTION_UPLOAD_VIDEO, ACTION_SMT_ELSE})
public @interface S3Action {
    int ACTION_NOT_DEFINED = -1;
    int ACTION_UPLOAD_VIDEO = 0;
    int ACTION_SMT_ELSE = 1;
}
