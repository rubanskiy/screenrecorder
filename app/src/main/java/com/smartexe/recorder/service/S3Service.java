package com.smartexe.recorder.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.smartexe.recorder.data.local.VideosRepository;
import com.smartexe.recorder.data.model.Video;
import com.smartexe.recorder.utils.NetworkUtils;

import java.io.File;
import java.util.List;

public class S3Service extends IntentService {

    private static final String TAG = S3Service.class.getSimpleName();

    private static final String ACTION = "ACTION";

    public static final String ARG_RECORD_SESSION = "ARG_RECORD_SESSION";

    public static final String IDENTITY = "us-east-1:5c92b208-f784-4025-aedb-4f3592b675ce";
    private static final String DEFAULT_BUCKET = "blink-dev-user-content";

    public static Intent getIntent(Context context, @S3Action int action, Bundle extras) {
        Intent intent = new Intent(context, S3Service.class);
        if (extras != null) {
            intent.putExtras(extras);
        }
        intent.putExtra(ACTION, action);
        return intent;
    }

    public S3Service() {
        super(S3Service.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (!NetworkUtils.isConnected(this)) {
            return;
        }

        @S3Action int action = intent.getIntExtra(ACTION, S3Action.ACTION_NOT_DEFINED);
        if (action == S3Action.ACTION_NOT_DEFINED) {
            return;
        }

        switch (action) {
            case S3Action.ACTION_UPLOAD_VIDEO: {
                if (!intent.getExtras().containsKey(ARG_RECORD_SESSION)) {
                    return;
                }

                String session = intent.getExtras().getString(ARG_RECORD_SESSION);
                VideosRepository repository = new VideosRepository(this);
                List<Video> videos = repository.query(session);

                if (TextUtils.isEmpty(session) || videos.isEmpty()) {
                    return;
                }

                ClientConfiguration clientConfiguration = new ClientConfiguration();
                clientConfiguration.setConnectionTimeout(10000);
                clientConfiguration.setCurlLogging(true);

                CognitoCachingCredentialsProvider credentials =
                        new CognitoCachingCredentialsProvider(this, IDENTITY, Regions.US_EAST_1, clientConfiguration);
                AmazonS3Client s3Client = new AmazonS3Client(credentials, Region.getRegion(Regions.US_EAST_1));

                for (Video video : videos) {
                    File fileToUpload = new File(video.getPath());
                    if (!fileToUpload.exists()) {
                        continue;
                    }

                    String key = "temp-uploads/" +  credentials.getCachedIdentityId() +
                            "/smartexe/video_" + video.getName().toLowerCase();
                    PutObjectRequest request = new PutObjectRequest(DEFAULT_BUCKET, key, fileToUpload);

                    try {
                        PutObjectResult response = s3Client.putObject(request);

                        Log.d(TAG, "Uploading to " + key);

                        if (response.getMetadata().getRawMetadata().containsKey("X-Android-Response-Source")) {
                            String code = (String) response.getMetadata().getRawMetadata().get("X-Android-Response-Source");
                            if (code.contains("200")) {
                                video.setUploaded(true);
                                repository.update(video);
                            } else {
                                // smt goes wrong. Try one more time (maybe later).
                            }
                        }
//                        https://github.com/aws-amplify/aws-sdk-android/issues/423
                    } catch (Exception e) {
                        e.printStackTrace();
//                        First time only
//                        Access Denied (Service: Amazon S3; Status Code: 403; Error Code: AccessDenied;
                    }
                }
                break;
            }
            case S3Action.ACTION_SMT_ELSE: {
                break;
            }
            default:
                break;
        }

    }

}
