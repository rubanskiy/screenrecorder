package com.smartexe.recorder.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.projection.MediaProjection;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.smartexe.recorder.R;
import com.smartexe.recorder.data.model.ScreenConfig;
import com.smartexe.recorder.ui.activity.RecordActivity;
import com.smartexe.recorder.utils.recorder.CodecScreenRecorder;
import com.smartexe.recorder.utils.recorder.ScreenRecorder;

import java.util.Random;
import java.util.UUID;

public class RecordService extends Service {

    private final static String TAG = RecordService.class.getSimpleName();

    private static final String CHANNEL_ID = "CHANNEL_ID";
    private static final String CHANNEL_NAME = "CHANNEL_NAME";

    private final static int VIDEO_CHUNK_TIME = 30000;
    private final static int PREPARE_FILE_DELAY = 1000;

    private NotificationManager notificationManager;

    private ScreenRecorder recorder;
    private String recordSession;

    private Handler handler;
    private Repeater repeater;
    private Preparer preparer;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        createChannel();

        int notificationId = (int) (System.currentTimeMillis() % 10000);
        Notification notification = createNotification(this);
        startForeground(notificationId, notification);

        recorder = new CodecScreenRecorder(this);
//        recorder = new MediaScreenRecorder(this);

        recordSession = UUID.randomUUID().toString();
        recorder.setRecordSession(recordSession);

        handler = new Handler();
        repeater = new Repeater();
        preparer = new Preparer();
    }

    private void createChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

    private Notification createNotification(Context context) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(context.getString(R.string.service_title))
                .setContentText(context.getString(R.string.service_message));

        Intent intent = new Intent(context, RecordActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context, new Random().nextInt(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(pendingIntent);

        return builder.build();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new RecordBinder();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recorder.stopRecord();
        handler.removeCallbacks(repeater);
        handler.removeCallbacks(preparer);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public void startRecord() {
        recorder.startRecord();
        recorder.prepareFile();
        handler.postDelayed(repeater, VIDEO_CHUNK_TIME);
    }

    public void setConfig(ScreenConfig config) {
        recorder.setConfig(config);
    }

    public void setProjection(MediaProjection projection) {
        recorder.setProjection(projection);
    }

    public String getRecordSession() {
        return recordSession;
    }

    public class RecordBinder extends Binder {

        public RecordService getRecordService() {
            return RecordService.this;
        }
    }

    private class Repeater implements Runnable {

        @Override
        public void run() {
            recorder.split();
            handler.postDelayed(this, VIDEO_CHUNK_TIME);
            handler.postDelayed(preparer, VIDEO_CHUNK_TIME - PREPARE_FILE_DELAY);
        }
    }

    private class Preparer implements Runnable {

        @Override
        public void run() {
            recorder.prepareFile();
        }
    }

}
