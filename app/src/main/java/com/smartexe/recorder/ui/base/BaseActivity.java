package com.smartexe.recorder.ui.base;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.smartexe.recorder.R;

public abstract class BaseActivity extends AppCompatActivity implements BaseMvpView {

    @LayoutRes
    protected abstract int getLayoutResource();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    @Override
    public void setTitleRes(int title) {

    }

    @Override
    public void showMessage(@StringRes int message) {
        Toast.makeText(this, getString(message), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onError(@StringRes int error) {
        Toast.makeText(this, getString(error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoNetwork() {
        onError(R.string.error_network);
    }

    public boolean askForPermissions(String[] permissions, int requestCode) {
        if (!hasPermissions(permissions)) {
            ActivityCompat.requestPermissions(BaseActivity.this, permissions, requestCode);
            return false;
        }
        return true;
    }

    private boolean hasPermissions(String... permissions) {
        if (permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

}
