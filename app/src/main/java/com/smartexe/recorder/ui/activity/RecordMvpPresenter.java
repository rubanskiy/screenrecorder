package com.smartexe.recorder.ui.activity;

import com.smartexe.recorder.ui.base.BaseMvpPresenter;
import com.smartexe.recorder.ui.base.BaseMvpView;

public interface RecordMvpPresenter<V extends BaseMvpView> extends BaseMvpPresenter<V> {

    void rebindIfNeed();

}
