package com.smartexe.recorder.ui.base;

import android.content.Context;

import androidx.annotation.StringRes;

import com.smartexe.recorder.R;

import java.net.UnknownHostException;

public class BasePresenter<V extends BaseMvpView> implements BaseMvpPresenter<V> {

    private V mMvpView;

    @Override
    public void attachTo(V view) {
        mMvpView = view;
    }

    @Override
    public void detach() {
        mMvpView = null;
    }

    @Override
    public V getMvpView() {
        return mMvpView;
    }

    @Override
    public Context getContext() {
        return getMvpView().getContext();
    }

    @Override
    public void handleError(Throwable error, @StringRes int defaultMessage) {
        if (error instanceof UnknownHostException) {
            getMvpView().showMessage(R.string.error_server);
        }

    }

}
