package com.smartexe.recorder.ui.base;

import android.content.Context;

import androidx.annotation.StringRes;

public interface BaseMvpView {

    Context getContext();

    void setTitleRes(@StringRes int error);

    void showMessage(@StringRes int resId);

    void onError(@StringRes int error);

    void onNoNetwork();

}
