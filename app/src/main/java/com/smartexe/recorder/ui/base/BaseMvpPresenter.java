package com.smartexe.recorder.ui.base;

import android.content.Context;

import androidx.annotation.StringRes;

public interface BaseMvpPresenter<V extends BaseMvpView> {

    void attachTo(final V view);

    void detach();

    V getMvpView();

    Context getContext();

    void handleError(Throwable error, @StringRes int defaultMessage);

}
