package com.smartexe.recorder.ui.activity;

import com.smartexe.recorder.ui.base.BasePresenter;
import com.smartexe.recorder.utils.ServiceUtils;

public class RecordPresenter <V extends RecordMvpView> extends BasePresenter<V>
        implements RecordMvpPresenter<V> {

    @Override
    public void rebindIfNeed() {
        if (ServiceUtils.isRecorderRunning(getContext())) {
            getMvpView().onRebind();
        }
    }

}
