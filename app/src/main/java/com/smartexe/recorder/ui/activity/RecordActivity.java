package com.smartexe.recorder.ui.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.smartexe.recorder.R;
import com.smartexe.recorder.data.model.ScreenConfig;
import com.smartexe.recorder.service.RecordService;
import com.smartexe.recorder.service.S3Action;
import com.smartexe.recorder.service.S3Service;
import com.smartexe.recorder.ui.base.BaseActivity;

public class RecordActivity extends BaseActivity implements RecordMvpView {

    private static final int RECORD_REQUEST_CODE = 1;
    private static final int ALL_PERMISSIONS_REQUEST_CODE = 2;
    private static final String[] permissions = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.RECORD_AUDIO
    };

    private Button btnStartStop;

    private RecordPresenter<RecordActivity> presenter = new RecordPresenter<>();

    private MediaProjectionManager projectionManager;
    private RecordService service;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_record;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachTo(this);

        projectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);

        btnStartStop = findViewById(R.id.btn_start_stop);
        btnStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (service != null) {
                    String recordSession = service.getRecordSession();
                    Bundle extras = new Bundle(1);
                    extras.putString(S3Service.ARG_RECORD_SESSION, recordSession);
                    Intent intent = S3Service.getIntent(RecordActivity.this, S3Action.ACTION_UPLOAD_VIDEO, extras);
                    startService(intent);

                    stopRecordService();
                    onServiceRunning(false);
                } else {
                    if (askForPermissions(permissions, ALL_PERMISSIONS_REQUEST_CODE)) {
                        startRecordService();
                        startActivityForResult(projectionManager.createScreenCaptureIntent(), RECORD_REQUEST_CODE);
                    }
                }
            }
        });
        presenter.rebindIfNeed();
    }

    @Override
    public void onServiceRunning(boolean isRunning) {
        int status = isRunning ? R.string.stop_recording : R.string.start_recording;
        btnStartStop.setText(status);
    }

    @Override
    public void onRebind() {
        bindService(new Intent(this, RecordService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (service != null) {
            unbindService(serviceConnection);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int granted = 0;
        for (int result : grantResults) {
            if (result == PackageManager.PERMISSION_GRANTED) {
                granted ++;
            }
        }
        if (granted == grantResults.length) {
            startRecordService();
            startActivityForResult(projectionManager.createScreenCaptureIntent(), RECORD_REQUEST_CODE);
        } else {
            showMessage(R.string.error_permission);
        }
    }

    private void startRecordService() {
        Intent recorder = new Intent(this, RecordService.class);
        bindService(recorder, serviceConnection, BIND_AUTO_CREATE);
        startService(recorder);
    }

    private void stopRecordService() {
        unbindService(serviceConnection);
        service = null;
        stopService(new Intent(RecordActivity.this, RecordService.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RECORD_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                MediaProjection mediaProjection = projectionManager.getMediaProjection(resultCode, data);
                if (service != null && mediaProjection != null) {
                    service.setProjection(mediaProjection);
                    service.startRecord();
                    moveTaskToBack(true);
                }
            } else {
                if (service != null) {
                    stopRecordService();
                    onServiceRunning(false);
                }
                showMessage(R.string.error_permission);
            }
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            RecordService.RecordBinder binder = (RecordService.RecordBinder) iBinder;
            service = binder.getRecordService();

            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            ScreenConfig screenConfig = new ScreenConfig();
            screenConfig.setWidth(metrics.widthPixels);
            screenConfig.setHeight(metrics.heightPixels);
            screenConfig.setDensity(metrics.densityDpi);

            service.setConfig(screenConfig);
            onServiceRunning(true);
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            onServiceRunning(false);
            stopService(new Intent(RecordActivity.this, RecordService.class));
        }

    };

}
