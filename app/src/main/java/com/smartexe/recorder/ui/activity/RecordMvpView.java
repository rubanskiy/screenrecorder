package com.smartexe.recorder.ui.activity;

import com.smartexe.recorder.ui.base.BaseMvpView;

public interface RecordMvpView extends BaseMvpView {

    void onServiceRunning(boolean isRunning);

    void onRebind();

}
